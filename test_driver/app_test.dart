// Imports the Flutter Driver API.

import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

extension Type on FlutterDriver {
  Future<void> type(SerializableFinder element, String text) async {
    await tap(element);
    await enterText(text);
  }
}

void main() {
  group('Testing App', () {
    FlutterDriver driver;

    final sampleEmail = 'max.mustermann@email.com';
    final signinLabel = 'Sign In';
    final samplePassword = 'Totally Safe Uncrackable password';
    final createAccountLabel = 'Create account';

    // finders_signin
    final signinLabel = 'Sign In';
    final signinFinder = find.byValueKey(signinLabel);
    final loginFinder = find.text('Log In');
    final passwordFinder = find.byValueKey('PASSWORD');
    final okFinder = find.descendant(
        of: find.byType('AlertDialog'), matching: find.byValueKey('OK'));
    final backFinder = find.pageBack();
    final emailFinder = find.byValueKey('EMAIL');
    final hamburgerFinder = find.byTooltip('Open navigation menu');
    final logoutFinder = find.text('Log Out');

    // finders_signup
    final createAccountFinder = find.byValueKey(createAccountLabel);
    final nextFinder = find.text('Next');

    final textFieldFinder = find.byType('TextFormField');

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      // test_setupall
      driver = await FlutterDriver.connect();
      var connected = false;
      while (!connected) {
        try {
          await driver.waitUntilFirstFrameRasterized();
          connected = true;
        } catch (error) {}
      }
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('can log in', () async {
      // given
      // click on sign in button
      await driver.tap(signinFinder);

      // enter email
      // find email field
      // tap on email field
      // enter text on email
      await driver.type(emailFinder, sampleEmail);

      // enter password
      await driver.type(passwordFinder, samplePassword);

      // when
      // you are logged out
      final isLoggedOut =
          await driver.requestData('signInWithEmailAndPassword');
      assert(isLoggedOut == 'false');

      // click on login
      await driver.tap(loginFinder);

      // then
      // you are logged
      final isLoggedIn = await driver.requestData('signInWithEmailAndPassword');
      assert(isLoggedIn == 'true');

      // logout
      // click on the hamburger icon
      await driver.tap(hamburgerFinder);
      await driver.tap(logoutFinder);
    });

    test('can sign up', () async {
      // test_signup

      await driver.tap(createAccountFinder);
      await driver.type(textFieldFinder, sampleEmail);
      await driver.tap(nextFinder);
      await driver.type(textFieldFinder, samplePassword);

      var createIsCalled =
          await driver.requestData('createUserWithEmailAndPassword');
      expect(createIsCalled, 'false');

      await driver.tap(nextFinder);

      createIsCalled =
          await driver.requestData('createUserWithEmailAndPassword');
      expect(createIsCalled, 'true');

      await driver.tap(okFinder);

      expect(await driver.getText(emailFinder), sampleEmail);

      await driver.tap(backFinder);
    });
  });
}
