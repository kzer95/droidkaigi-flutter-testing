import 'package:droidkaigi_testing/domain/User.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:droidkaigi_testing/main.dart' as app;
import 'package:mockito/mockito.dart';

import 'MockClient.dart';

import 'package:flutter/scheduler.dart';

void main() {
  var createdUser = false;
  var loginCalled = false;

  Future<String> dataHandler(String msg) async {
    switch (msg) {
      // handler_createuser
      case 'createUserWithEmailAndPassword':
        return createdUser.toString();
      // handler_signIn
      case 'signInWithEmailAndPassword':
        return loginCalled.toString();
      default:
        return '';
    }
  }

  // driver
  enableFlutterDriverExtension(handler: dataHandler);

  // mock_login
  final mockClient = MockClient();
  when(mockClient.isLoggedIn()).thenAnswer((_) async => false);
  when(mockClient.signInWithEmailAndPassword(
          email: anyNamed('email'), password: anyNamed('password')))
      .thenAnswer((_) async {
    loginCalled = true;
    return MockAuthResult();
  });
  when(mockClient.getUser()).thenAnswer((_) async {
    if (loginCalled) {
      return User(email: 'email');
    }
    return null;
  });
  // mock_createUser
  when(mockClient.createUserWithEmailAndPassword(
          email: anyNamed('email'), password: anyNamed('password')))
      .thenAnswer((_) async {
    createdUser = true;
    return MockAuthResult();
  });

  // timedilation
  timeDilation = 0.00001;

  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  // mock_state
  app.state = MockHomeStateProvider(client: mockClient, user: null);
  // mock_state_testing
  app.state.isTesting = true;

  app.main();
}
