import 'package:droidkaigi_testing/data/FirebaseRepository.dart';
import 'package:droidkaigi_testing/domain/CinephileRepository.dart';
import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mockito/mockito.dart';

class MockAuthResult extends Mock implements AuthResult {
  @override
  AdditionalUserInfo get additionalUserInfo => null;

  @override
  FirebaseUser get user => MockFirebaseUser();
}

class MockFirebaseUser extends Mock implements FirebaseUser {}

class MockHomeStateProvider extends HomeStateProvider {
  MockHomeStateProvider({CinephileRepository client, User user})
      : super(client: client, user: user);
}

class MockClient extends Mock implements FirebaseRepository {}
