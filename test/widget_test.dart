// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:droidkaigi_testing/domain/CinephileRepository.dart';
import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/main.dart';
import 'package:droidkaigi_testing/models/Movie.dart';
import 'package:droidkaigi_testing/views/pages/MoviesListPage.dart';
import 'package:droidkaigi_testing/widgets/AvatarImageView.dart';
import 'package:droidkaigi_testing/widgets/DebouncedTextField.dart';
import 'package:droidkaigi_testing/widgets/MovieListItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mockito/mockito.dart';
import 'dart:ui' show window;

import 'package:provider/provider.dart';

// mock client class
class MockClient extends Mock implements CinephileRepository {}

// mock observer class
class MockNavigatorObserver extends Mock implements NavigatorObserver {}

// test_materialcontainer
Widget buildMaterialContainer(
    {child: Widget, List<NavigatorObserver> navigatorObservers, User user}) {
  final data = MediaQueryData.fromWindow(window);

  return MediaQuery(
    data: data,
    child: buildMaterialApp(
      currentUser: user,
      observers: navigatorObservers,
      child: child,
    ),
  );
}

// test_widgetcontainer
Widget buildWidgetContainer({child: Widget}) {
  return Directionality(
    textDirection: TextDirection.ltr,
    child: StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        return MediaQuery(
          data: MediaQueryData.fromWindow(window),
          child: Material(
            child: Center(
              child: child,
            ),
          ),
        );
      },
    ),
  );
}

void main() {
  final user = User();
  // mock client
  final mockClient = MockClient();
  // mock home state
  final mockHomeState = HomeStateProvider(client: mockClient, user: user);
  // mock observer
  NavigatorObserver mockObserver;

  final movieName = 'this is the name';
  final movieName2 = 'this is not the name';
  final sampleMovie = Movie(
    id: 0,
    imageUrl: '',
    name: movieName,
    year: '2020',
    watchedTimes: 3,
  );
  final movieWithoutWatched = Movie(
    id: 0,
    name: movieName2,
    imageUrl: '',
    year: '2020',
  );

  setUp(() {
    mockObserver = MockNavigatorObserver();
  });
  testWidgets(
      'test MovieListItem 1 with find byKey, text, semanticsLabel, byWidget, match none, match one',
      (WidgetTester tester) async {
    // test_item
    final itemRow = MovieListItem(item: sampleMovie);
    await tester.pumpWidget(
      buildWidgetContainer(
        child: Center(
          child: itemRow,
        ),
      ),
    );

    expect(find.byKey(MovieListItem.watchedTimes), findsOneWidget);
    expect(find.bySemanticsLabel(MovieListItem.yearKey), findsOneWidget);
    expect(find.text(movieName), findsOneWidget);
    expect(find.text(movieName2), findsNothing);
    expect(find.byType(AvatarImageView), findsOneWidget);
    expect(find.byWidget(itemRow.imageView), findsOneWidget);
  });

  testWidgets('test MovieListItem 2 without year', (WidgetTester tester) async {
    // test_item2
    final itemRow = MovieListItem(item: movieWithoutWatched);

    await tester.pumpWidget(
      buildWidgetContainer(
        child: Center(
          child: itemRow,
        ),
      ),
    );

    expect(find.byKey(MovieListItem.watchedTimes), findsNothing);
    expect(find.bySemanticsLabel(MovieListItem.yearKey), findsOneWidget);
    expect(find.text(movieName2), findsOneWidget);
    expect(find.text(movieName), findsNothing);
    expect(find.byType(AvatarImageView), findsOneWidget);
  });

  testWidgets('stateful widget, using drag gesture, should reveal bottom rows',
      (WidgetTester tester) async {
    // test_scroll
    when(mockClient.watchedMovies(null)).thenAnswer(
      (_) async => [
        sampleMovie,
        sampleMovie,
        sampleMovie,
        sampleMovie,
        sampleMovie,
        sampleMovie,
        sampleMovie,
        movieWithoutWatched,
        movieWithoutWatched,
        movieWithoutWatched,
        movieWithoutWatched,
      ],
    );

    await tester.pumpWidget(
      buildMaterialContainer(
        child: ChangeNotifierProvider<HomeStateProvider>.value(
          value: mockHomeState,
          child: Consumer<HomeStateProvider>(
            builder: (context, counter, _) {
              return MoviesListPage();
            },
          ),
        ),
        navigatorObservers: [mockObserver],
      ),
    );

    await tester.pumpAndSettle();

    expect(find.text(movieName), findsWidgets);
    expect(find.text(movieName2), findsNWidgets(0));

    final lastRow = find.text(movieName).first;
    expect(lastRow, findsNWidgets(1));

    final lastRowCenter = tester.getCenter(lastRow);

    await tester.dragFrom(lastRowCenter, Offset(0, -100));
    await tester.pumpAndSettle();

    expect(find.text(movieName2), findsWidgets);
  });

  testWidgets('stateful widget, enterText should be able to search',
      (WidgetTester tester) async {
    // test_search
    when(mockClient.watchedMovies(null)).thenAnswer(
      (_) async => [],
    );

    await tester.pumpWidget(
      buildMaterialContainer(
        child: ChangeNotifierProvider<HomeStateProvider>.value(
          value: mockHomeState,
          child: Consumer<HomeStateProvider>(
            builder: (context, counter, _) {
              return MoviesListPage();
            },
          ),
        ),
        navigatorObservers: [mockObserver],
      ),
    );

    await tester.pumpAndSettle();

    final searchQuery = 'I am searching for this';
    final searchPlaceholder = 'Search your watch history';
    await tester.enterText(
        find.widgetWithText(DebouncedTextField, searchPlaceholder),
        searchQuery);
    await tester.pumpAndSettle(Duration(seconds: 1));

    expect(find.text(searchQuery), findsOneWidget);
  });

  testWidgets('stateful widget, tap should be able to add',
      (WidgetTester tester) async {
    // test_add
    when(mockClient.watchedMovies(null)).thenAnswer(
      (_) async => [],
    );

    await tester.pumpWidget(
      buildMaterialContainer(
        child: ChangeNotifierProvider<HomeStateProvider>.value(
          value: mockHomeState,
          child: Consumer<HomeStateProvider>(
            builder: (context, counter, _) {
              return MoviesListPage();
            },
          ),
        ),
        navigatorObservers: [mockObserver],
      ),
    );

    await tester.pumpAndSettle();
    await tester.tap(find.byType(FloatingActionButton));

    verify(mockObserver.didPush(any, any));
  });
}
