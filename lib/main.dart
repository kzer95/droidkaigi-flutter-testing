import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:droidkaigi_testing/data/FirebaseRepository.dart';
import 'package:droidkaigi_testing/data/OMDBClient.dart';
import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/Routes.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/domain/WaitingNotifier.dart';
import 'package:droidkaigi_testing/models/Movie.dart';
import 'package:droidkaigi_testing/views/pages/AddMoviePage.dart';
import 'package:droidkaigi_testing/views/pages/EmailSignupPage.dart';
import 'package:droidkaigi_testing/views/pages/ItemDetailPage.dart';
import 'package:droidkaigi_testing/views/pages/LoginPage.dart';
import 'package:droidkaigi_testing/views/pages/MoviesListPage.dart';
import 'package:droidkaigi_testing/views/pages/WelcomePage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';

HomeStateProvider state;
const apiKey = '57718d96';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final FirebaseApp app = await FirebaseApp.configure(
    name: 'ritual',
    options: const FirebaseOptions(
      googleAppID: '1:302279181765:ios:9945461da63584708437ac',
      gcmSenderID: '302279181765',
      apiKey: 'AIzaSyDZlmrRU-00xom0EmtEp2H2XgsLiJvRceQ',
      projectID: 'droidkaigi-flutter-testing',
    ),
  );

  final bool isInDebugMode = false;

  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Crashlytics.
      // Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  runZonedGuarded<Future<Null>>(() async {
    final firestore = Firestore(app: app);
    firestore.settings(persistenceEnabled: false);

    runApp(
      MyApp(
        state ??
            HomeStateProvider(
                client: FirebaseRepository(OMDBClient(apiKey)), user: null),
      ),
    );
  }, (error, stackTrace) async {
    print(error);
  });
}

class MyApp extends StatelessWidget {
  MyApp(this._state);

  final HomeStateProvider _state;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
        future: _state.client.getUser(),
        builder: (context, snapshot) {
          final currentUser = snapshot.data;
          _state.user = currentUser;

          return ChangeNotifierProvider<HomeStateProvider>.value(
            value: _state,
            child: Consumer<HomeStateProvider>(
              builder: (context, counter, _) {
                return buildMaterialApp(
                  currentUser: currentUser,
                  observers: null,
                  child: currentUser == null
                      ? WelcomePage()
                      : ChangeNotifierProvider<HomeStateProvider>.value(
                          value: _state,
                          child: Consumer<HomeStateProvider>(
                            builder: (context, counter, _) {
                              return MoviesListPage();
                            },
                          ),
                        ),
                );
              },
            ),
          );
        });
  }
}

Widget buildMaterialApp({
  User currentUser,
  List<NavigatorObserver> observers,
  Widget child,
}) {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    navigatorObservers: observers ?? [],
    theme: ThemeData(
      fontFamily: 'Lato',
      primarySwatch: MaterialColor(0xff1FAF9F, {
        50: Color(0xff1FAF9F),
        100: Color(0xff1FAF9F),
        200: Color(0xff1FAF9F),
        300: Color(0xff1FAF9F),
        400: Color(0xff1FAF9F),
        500: Color(0xff1FAF9F),
        600: Color(0xff1FAF9F),
        700: Color(0xff1FAF9F),
        800: Color(0xff1FAF9F),
        900: Color(0xff1FAF9F),
      }),
    ),
    home: child,
    onGenerateRoute: (settings) {
      return MaterialPageRoute(
        builder: (context) {
          return _makeRoute(
              context: context,
              routeName: settings.name,
              arguments: settings.arguments);
        },
        maintainState: true,
        fullscreenDialog: false,
      );
    },
  );
}

Widget _makeRoute(
    {@required BuildContext context,
    @required String routeName,
    Object arguments}) {
  final Widget child = _buildRoute(
    context: context,
    routeName: routeName,
    arguments: arguments,
  );
  return child;
}

Widget _buildRoute({
  @required BuildContext context,
  @required String routeName,
  Object arguments,
}) {
  final HomeStateProvider homeState = Provider.of<HomeStateProvider>(context);

  switch (routeName) {
    case Routes.welcome:
      return WelcomePage();
    case Routes.login:
      final map = arguments as Map<String, dynamic> ?? Map();
      final String email = map['email'] as String;
      final String password = map['password'] as String;

      return ChangeNotifierProvider<WaitingNotifier>.value(
        value: WaitingNotifier(),
        child: LoginPage(email: email, password: password),
      );
    case Routes.signup:
      return ChangeNotifierProvider<WaitingNotifier>.value(
        value: WaitingNotifier(),
        child: EmailSignupPage(User()),
      );
    case Routes.add:
      return ChangeNotifierProvider<HomeStateProvider>.value(
        value: homeState,
        child: AddMoviePage(),
      );
    case Routes.details:
      final map = arguments as Map<String, dynamic> ?? Map();
      final item = map['item'] as Movie;

      return MultiProvider(
        providers: [
          ChangeNotifierProvider<HomeStateProvider>.value(
            value: homeState,
          )
        ],
        child: ItemDetailPage(item: item),
      );
    case Routes.home:
      final user = arguments as User;
      homeState.user = user;

      return ChangeNotifierProvider<HomeStateProvider>.value(
        value: homeState,
        child: Consumer<HomeStateProvider>(
          builder: (context, counter, _) {
            return MoviesListPage();
          },
        ),
      );
    default:
      throw 'Route $routeName is not defined';
  }
}
