class Routes {
  static const login = '/login';
  static const welcome = '/welcome';
  static const signup = '/signup';
  static const home = '/home';
  static const add = '/add';
  static const details = '/details';
}
