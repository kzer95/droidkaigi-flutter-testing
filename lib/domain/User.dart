import 'package:shared_preferences/shared_preferences.dart';
import 'package:json_annotation/json_annotation.dart';

part 'User.g.dart';

@JsonSerializable()
class User {
  static const UserUid = 'UserUid';

  factory User.fromJson(Map json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  var email;
  var id;

  var _tempPassword = "";
  String get tempPassword => _tempPassword;
  void setTempPassword(String p) {
    _tempPassword = p;
  }

  User({
    this.email,
  });

  static Future<void> setLoggedInUserUid(String uid) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (uid == null)
      await prefs.remove(UserUid);
    else
      await prefs.setString(UserUid, uid);
  }

  static Future<String> loggedInUserUid() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(UserUid);
  }
}
