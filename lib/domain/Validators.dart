class Validators {
  static String validateEmail(String value) {
    final Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    final RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) return 'Please enter a valid Email';

    return null;
  }

  static String validatePassword(String value) {
    if (value.length < 6) return 'Please enter at least 6 characters';

    return null;
  }

  static String validateNotEmpty(String value) {
    if (value.length < 1) return 'Should not be empty';

    return null;
  }
}
