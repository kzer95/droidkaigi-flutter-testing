import 'package:cloud_firestore/cloud_firestore.dart';

typedef CollectionReference CollectionGet(String path);

class DatabaseRepository {
  CollectionGet collectionGet;

  DatabaseRepository(this.collectionGet);
}
