import 'package:flutter/material.dart';

class WaitingNotifier with ChangeNotifier {
  WaitingNotifier();

  var _isLoading = false;

  bool get isLoading => _isLoading;
  set isLoading(bool loading) {
    _isLoading = loading;
    notifyListeners();
  }
}
