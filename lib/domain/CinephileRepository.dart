import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/models/Movie.dart';
import 'package:droidkaigi_testing/models/MovieSearchResponse.dart';
import 'package:firebase_auth/firebase_auth.dart';

abstract class CinephileRepository {
  /// Auth
  Future<AuthResult> signInWithEmailAndPassword(
      {String email, String password});
  Future<AuthResult> createUserWithEmailAndPassword(
      {String email, String password});
  Future<void> signOut();

  // Movies
  Future<MovieSearchResponse> findMovies(String query);
  Future<void> addToWatched(Movie movie);
  Future<List<Movie>> watchedMovies(String query);
  Future<void> removeFromWatched(Movie movie);

  Future<User> getUser();
  Future<bool> isLoggedIn();
  Future<User> createUser(String id, User user);
}
