import 'package:droidkaigi_testing/domain/CinephileRepository.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:flutter/material.dart';

class HomeStateProvider with ChangeNotifier {
  HomeStateProvider({this.client, this.user});

  CinephileRepository client;
  User user;

  bool isTesting = false;

  bool hasCreatedUser;

  Future<void> signOut() async {
    hasCreatedUser = false;
    try {
      await User.setLoggedInUserUid(null);
      await client.signOut();
    } catch (e) {}
  }

  void refresh() {
    notifyListeners();
  }
}
