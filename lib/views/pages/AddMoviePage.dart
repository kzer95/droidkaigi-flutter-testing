import 'package:flutter/material.dart';
import 'package:droidkaigi_testing/models/Movie.dart';
import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/widgets/DebouncedTextField.dart';
import 'package:droidkaigi_testing/widgets/MovieListItem.dart';
import 'package:provider/provider.dart';
import 'package:droidkaigi_testing/domain/MovieSearchResponse+DataToDomain.dart';

class AddMoviePage extends StatefulWidget {
  @override
  _AddMoviePageState createState() => _AddMoviePageState();
}

class _AddMoviePageState extends State<AddMoviePage> {
  final controller = TextEditingController();
  List<Movie> _results = [];

  @override
  Widget build(BuildContext context) {
    final HomeStateProvider app = Provider.of<HomeStateProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Add Movie'),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: DebouncedTextField(
              debounceDuration: Duration(milliseconds: 500),
              decoration:
                  InputDecoration(hintText: 'Add a movie you have seen before'),
              controller: controller,
              onChanged: (v) async {
                final results = await app.client.findMovies(v);

                setState(() {
                  _results = results.toDomain();
                });
              },
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: _results.length,
              itemBuilder: (context, index) {
                final item = _results[index];
                return GestureDetector(
                  onTap: () => _markAsWatched(item),
                  child: MovieListItem(item: item),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  void _markAsWatched(Movie movie, [DateTime dateWatched]) async {
    final HomeStateProvider app = Provider.of<HomeStateProvider>(context);

    final date = dateWatched ?? DateTime.now();
    movie.watchedOn = date.millisecondsSinceEpoch;
    await app.client.addToWatched(movie);

    app.refresh();
    Navigator.pop(context);
  }
}
