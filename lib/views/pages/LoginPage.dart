import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/domain/Validators.dart';
import 'package:droidkaigi_testing/domain/WaitingNotifier.dart';
import 'package:droidkaigi_testing/views/Alerts.dart';
import 'package:droidkaigi_testing/views/AppBar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  final String email;
  final String password;

  const LoginPage({Key key, this.email, this.password}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _passwordFocus = FocusNode();

  // final notifier = WaitingNotifier();
  get homeState => Provider.of<HomeStateProvider>(context);

  @override
  void initState() {
    super.initState();

    _email.text = widget.email;
    _password.text = widget.password;
  }

  @override
  Widget build(BuildContext context) {
    final notifier = Provider.of<WaitingNotifier>(context);

    return Scaffold(
      appBar: buildEmptyAppBar(context),
      body: Container(
        constraints: BoxConstraints.expand(),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 35,
              bottom: 27,
              child: Container(
                width: 330,
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          'Cinephile',
                          style: TextStyle(fontSize: 80),
                        ),
                      ),
                      Spacer(),
                      TextFormField(
                        key: ValueKey('EMAIL'),
                        autofocus: true,
                        controller: _email,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          labelText: "EMAIL",
                        ),
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).requestFocus(_passwordFocus),
                        validator: (value) => Validators.validateEmail(value),
                      ),
                      TextFormField(
                        key: ValueKey('PASSWORD'),
                        controller: _password,
                        obscureText: true,
                        focusNode: _passwordFocus,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          labelText: "PASSWORD",
                        ),
                        onFieldSubmitted: (_) =>
                            notifier.isLoading ? null : _login(),
                        validator: (value) =>
                            Validators.validatePassword(value),
                      ),
                      if (notifier.isLoading)
                        Center(
                          child: SizedBox(
                            width: 44.0,
                            height: 44.0,
                            child: const CircularProgressIndicator(),
                          ),
                        ),
                      Spacer(),
                      Align(
                        alignment: Alignment.topCenter,
                        child: FlatButton(
                          onPressed: () => notifier.isLoading
                              ? null
                              : Navigator.of(context).pop(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "New? Sign up",
                                style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          width: 330,
                          height: 50,
                          margin: EdgeInsets.only(bottom: 26),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            border: Border.all(color: Colors.white, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(25)),
                          ),
                          child: FlatButton(
                            onPressed: () =>
                                notifier.isLoading ? null : _login(),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Log In",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _login() async {
    final notifier = Provider.of<WaitingNotifier>(context);

    if (_formKey.currentState.validate()) {
      try {
        notifier.isLoading = true;

        final anEmail = _email.text;
        final aPassword = _password.text;

        final firebaseUser = await homeState.client
            .signInWithEmailAndPassword(email: anEmail, password: aPassword);

        await User.setLoggedInUserUid(firebaseUser.user.uid);

        final isVerified = true;

        if (isVerified) {
          final user = await homeState.client.getUser();

          notifier.isLoading = false;

          if (user == null) {
            Alerts.showAlert(
                context, "Please sign up again using the same password.");
            await homeState.signOut();
          } else {
            Navigator.of(context).pushNamedAndRemoveUntil("/home", (_) => false,
                arguments: user);
          }
        } else {
          await firebaseUser.user.sendEmailVerification();
          notifier.isLoading = false;

          Alerts.showAlert(context,
              "Please verify your email. We sent another verification email just in case.");
          await homeState.signOut();
        }
      } catch (e) {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text(e.toString()),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("OK"),
                      onPressed: () => Navigator.of(context).pop(),
                    )
                  ],
                ));

        notifier.isLoading = false;
      }
    }
  }
}
