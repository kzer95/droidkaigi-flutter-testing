import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/Routes.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/domain/Validators.dart';
import 'package:droidkaigi_testing/domain/WaitingNotifier.dart';
import 'package:droidkaigi_testing/views/Alerts.dart';
import 'package:droidkaigi_testing/views/AppBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SignupPage extends StatefulWidget {
  final String header;
  final List<String> fields;
  final User user;

  SignupPage({this.header, this.fields, this.user});

  @override
  SignupPageState createState() => SignupPageState();
}

class SignupPageState extends State<SignupPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  // final notifier = WaitingNotifier();
  final List<TextEditingController> controllers = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController()
  ];

  final List<FocusNode> focusNodes = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
  ];

  get homeState => Provider.of<HomeStateProvider>(context);

  void next(BuildContext context) async {}

  List<Widget> _buildFields(BuildContext context) {
    return widget.fields.map((field) {
      final index = widget.fields.indexOf(field);

      final formField = TextFormField(
        autofocus: index == 0,
        obscureText: field == "PASSWORD",
        controller: controllers[index],
        focusNode: focusNodes[index],
        decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelText: field,
        ),
        onFieldSubmitted: (_) => index == (widget.fields.length - 1)
            ? next(context)
            : FocusScope.of(context).requestFocus(focusNodes[index + 1]),
        validator: (value) => Validators.validateNotEmpty(value),
      );

      return formField;
    }).toList();
  }

  void createUser(BuildContext context) async {
    final notifier = Provider.of<WaitingNotifier>(context);

    if (notifier.isLoading) {
      return;
    }

    try {
      notifier.isLoading = true;
      final homeState = Provider.of<HomeStateProvider>(context);

      final newUser = await homeState.client.createUserWithEmailAndPassword(
          email: widget.user.email, password: widget.user.tempPassword);

      // Create a user
      await newUser.user.sendEmailVerification();

      homeState.client.createUser(newUser.user.uid, widget.user);
      notifier.isLoading = false;

      Alerts.showAlert(
          context, "Sign up Successful! Please verify your email to login.",
          () {
        Navigator.of(context).pushNamedAndRemoveUntil("/login", (route) {
          return route.settings.name == "/";
        }, arguments: {
          "email": widget.user.email,
          "password": widget.user.tempPassword
        });
      });
    } catch (e) {
      if (e is PlatformException && e.code == "ERROR_EMAIL_ALREADY_IN_USE") {
        _handleExisting(context);
      } else if (e is PlatformException) {
        Alerts.showAlert(context, e?.message);

        notifier.isLoading = false;
      } else {
        Alerts.showAlert(context, e);

        notifier.isLoading = false;
      }
    }
  }

  void _handleExisting(BuildContext context) async {
    final notifier = Provider.of<WaitingNotifier>(context);
    final homeState = Provider.of<HomeStateProvider>(context);

    try {
      // Retry
      final existingUser = await homeState.client.signInWithEmailAndPassword(
        email: widget.user.email,
        password: widget.user.tempPassword,
      );

      await User.setLoggedInUserUid(existingUser.user.uid);

      // Create a user
      await homeState.client.createUser(
        existingUser.user.uid,
        widget.user,
      );

      notifier.isLoading = false;

      final user = await homeState.client.getUser();

      homeState.hasCreatedUser = true;

      Navigator.of(context)
          .pushNamedAndRemoveUntil(Routes.home, (_) => false, arguments: user);
    } catch (e) {
      var message = "$e";

      await homeState.signOut();

      if (e is PlatformException) {
        message = e.message;
      }
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text(message),
                actions: <Widget>[
                  FlatButton(
                    child: Text("OK"),
                    onPressed: () => Navigator.of(context).pop(),
                  )
                ],
              ));

      notifier.isLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final notifier = Provider.of<WaitingNotifier>(context);

    return Scaffold(
      appBar: buildEmptyAppBar(context),
      body: Container(
        padding: EdgeInsets.all(26),
        constraints: BoxConstraints.expand(),
        child: Form(
          key: formKey,
          child: Stack(
            children: [
              Positioned(
                right: 0,
                bottom: 50,
                child: FlatButton(
                  onPressed: () => next(context),
                  child: Stack(
                    children: <Widget>[
                      Text(
                        'Next',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        widget.header,
                        style: TextStyle(
                          fontSize: 28,
                        ),
                        textAlign: TextAlign.left,
                      )),
                  ..._buildFields(context),
                  if (notifier.isLoading)
                    Center(
                      child: SizedBox(
                        width: 44.0,
                        height: 44.0,
                        child: const CircularProgressIndicator(),
                      ),
                    ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
