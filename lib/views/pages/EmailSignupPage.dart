import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/domain/WaitingNotifier.dart';
import 'package:droidkaigi_testing/views/pages/PasswordSignupPage.dart';
import 'package:droidkaigi_testing/views/pages/SignupPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EmailSignupPage extends SignupPage {
  EmailSignupPage(User user)
      : super(
            header: "Your business email?",
            fields: const ["Ex. howard@starbucks.com"],
            user: user);

  @override
  _EmailSignupPageState createState() => _EmailSignupPageState();
}

class _EmailSignupPageState extends SignupPageState {
  @override
  void next(BuildContext context) {
    if (formKey.currentState.validate()) {
      final updatedUser = widget.user;

      final email = controllers[0].text;
      updatedUser.email = email;

      final nextPage = ChangeNotifierProvider<WaitingNotifier>.value(
        value: WaitingNotifier(),
        child: PasswordSignupPage(updatedUser),
      );

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => nextPage));
    }
  }
}
