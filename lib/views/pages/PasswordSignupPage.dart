import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/views/pages/SignupPage.dart';
import 'package:flutter/material.dart';

class PasswordSignupPage extends SignupPage {
  PasswordSignupPage(User user)
      : super(
            header: "Set your password",
            fields: const ["PASSWORD"],
            user: user);

  @override
  _PasswordSignupPageState createState() => _PasswordSignupPageState();
}

class _PasswordSignupPageState extends SignupPageState {
  @override
  void next(BuildContext context) async {
    if (formKey.currentState.validate()) {
      final pass = controllers[0].text;
      widget.user.setTempPassword(pass);

      createUser(context);
    }
  }
}
