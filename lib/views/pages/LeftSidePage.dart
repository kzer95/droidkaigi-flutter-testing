import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/Routes.dart';
import 'package:droidkaigi_testing/views/FullSizeTextButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LeftSidePage extends StatefulWidget {
  @override
  _LeftSidePageState createState() => _LeftSidePageState();
}

class _LeftSidePageState extends State<LeftSidePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 0, left: 28, right: 28, bottom: 0.0),
              child: FullSizeTextButton(
                onPressed: () async {
                  final homeState = Provider.of<HomeStateProvider>(context);
                  homeState.signOut();

                  Navigator.of(context).pop();
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(Routes.welcome, (_) => false);
                },
                text: 'Log Out',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
