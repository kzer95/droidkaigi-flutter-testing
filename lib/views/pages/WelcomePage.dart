import 'package:droidkaigi_testing/domain/HomeStateProvider.dart';
import 'package:droidkaigi_testing/domain/Routes.dart';
import 'package:droidkaigi_testing/views/PillShapedButton.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  bool _isLoggedIn = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();

    final homeState = Provider.of<HomeStateProvider>(context);

    _isLoggedIn = await homeState.client.isLoggedIn();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 64,
              bottom: 27,
              child: Container(
                width: 330,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Stack(
                        children: <Widget>[
                          Text(
                            'Cinephile',
                            style: TextStyle(fontSize: 70),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    if (!_isLoggedIn)
                      Align(
                        alignment: Alignment.topCenter,
                        child: PillShapedButton(
                            key: ValueKey('Create account'),
                            width: 330,
                            height: 50,
                            onPressed: () =>
                                Navigator.of(context).pushNamed(Routes.signup),
                            text: 'Create account'),
                      ),
                    if (!_isLoggedIn)
                      Align(
                        alignment: Alignment.topCenter,
                        child: FlatButton(
                          key: ValueKey('Sign In'),
                          onPressed: () =>
                              Navigator.of(context).pushNamed(Routes.login),
                          child: Text(
                            'Sign In',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 18,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
