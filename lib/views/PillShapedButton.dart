import 'package:flutter/material.dart';

class PillShapedButton extends StatelessWidget {
  final Color backgroundColor;
  final bool hasShadow;
  final double width;
  final double height;
  final double fontSize;
  final String text;
  final FontWeight fontWeight;
  final VoidCallback onPressed;
  final Color textColor;

  const PillShapedButton(
      {Key key,
      this.width,
      this.height,
      this.text,
      this.onPressed,
      this.fontSize = 20,
      this.hasShadow = false,
      this.fontWeight = FontWeight.normal,
      this.backgroundColor,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: EdgeInsets.only(bottom: 26),
      decoration: BoxDecoration(
          color: backgroundColor ?? Theme.of(context).primaryColor,
          border: Border.all(color: Colors.white, width: 1),
          borderRadius: BorderRadius.all(Radius.circular(25)),
          boxShadow: [
            if (hasShadow)
              BoxShadow(
                blurRadius: 10.0,
                offset: Offset(0, 3),
                spreadRadius: -1,
                color: Color(0xff9B9B9B),
              ),
          ]),
      child: FlatButton(
        onPressed: onPressed,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              style: TextStyle(
                color: textColor ?? Colors.white,
                fontSize: fontSize,
                fontWeight: fontWeight,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
