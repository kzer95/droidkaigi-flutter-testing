import 'package:flutter/material.dart';

Widget buildEmptyAppBar(BuildContext context) {
  return AppBar(
    automaticallyImplyLeading: true,
    backgroundColor: Color(0x000000),
    elevation: 0,
    iconTheme: IconThemeData(
      color: Theme.of(context).primaryColor,
    ),
  );
}
