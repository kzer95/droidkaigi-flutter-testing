import 'package:flutter/material.dart';

class Alerts {
  static void showTBI(BuildContext context) {
    showAlert(context, "To be implemented");
  }

  static void showAlert(BuildContext context, String text,
      [VoidCallback onPressed, VoidCallback onNoPressed]) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(text),
        actions: <Widget>[
          FlatButton(
            key: ValueKey('OK'),
            child: Text(onNoPressed != null ? "Yes" : "OK"),
            onPressed: onPressed ?? () => Navigator.pop(context),
          ),
          if (onNoPressed != null)
            FlatButton(
              child: Text("No"),
              onPressed: onNoPressed ?? () => Navigator.pop(context),
            )
        ],
      ),
    );
  }
}
