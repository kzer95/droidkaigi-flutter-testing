import 'package:flutter/material.dart';

class FullSizeTextButton extends StatelessWidget {
  const FullSizeTextButton({
    Key key,
    @required this.text,
    @required this.fontSize,
    @required this.onPressed,
    FontWeight fontWeight,
  })  : _fontWeight = fontWeight ?? FontWeight.normal,
        super(key: key);

  final String text;
  final double fontSize;
  final VoidCallback onPressed;
  final FontWeight _fontWeight;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        padding: EdgeInsets.all(0),
        onPressed: onPressed,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(text,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: fontSize,
                  fontWeight: _fontWeight)),
        ),
      ),
    );
  }
}
