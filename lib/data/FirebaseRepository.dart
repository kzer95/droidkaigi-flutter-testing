import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:droidkaigi_testing/data/OMDBClient.dart';
import 'package:droidkaigi_testing/domain/CinephileRepository.dart';
import 'package:droidkaigi_testing/domain/DatabaseRepository.dart';
import 'package:droidkaigi_testing/domain/User.dart';
import 'package:droidkaigi_testing/models/Movie.dart';
import 'package:droidkaigi_testing/models/MovieSearchResponse.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseRepository extends CinephileRepository {
  static const kUsers = 'users';

  final OMDBClient omdbClient;

  static DatabaseRepository repository = DatabaseRepository(realCollection);

  static var realCollection = Firestore.instance.collection;

  FirebaseRepository(this.omdbClient);

  @override
  Future<AuthResult> signInWithEmailAndPassword(
      {String email, String password}) async {
    return await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
  }

  @override
  Future<AuthResult> createUserWithEmailAndPassword(
      {String email, String password}) async {
    return await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
  }

  @override
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Future<User> getUser() async {
    final auth = await User.loggedInUserUid();

    if (auth != null) {
      final queryResponse =
          await repository.collectionGet(kUsers).document(auth).get();

      final userMap = queryResponse.data;

      if (userMap == null) return null;

      final user = User.fromJson(userMap);

      return user;
    }

    return null;
  }

  @override
  Future<bool> isLoggedIn() async {
    final auth = await User.loggedInUserUid();
    return auth != null;
  }

  @override
  Future<User> createUser(String id, User user) async {
    user.id = id;

    final userMap = user.toJson();

    await repository.collectionGet(kUsers).document(id).setData(userMap);

    return user;
  }

  @override
  Future<void> addToWatched(Movie movie) async {
    final userId = await User.loggedInUserUid();
    await Firestore.instance
        .collection('users')
        .document(userId)
        .collection('watchlist')
        .document(movie.imdbId)
        .setData(movie.toMap());
  }

  @override
  Future<void> removeFromWatched(Movie movie) async {
    final userId = await User.loggedInUserUid();
    await Firestore.instance
        .collection('users')
        .document(userId)
        .collection('watchlist')
        .document(movie.imdbId.toString())
        .delete();
  }

  @override
  Future<List<Movie>> watchedMovies(String query) async {
    final userId = await User.loggedInUserUid();
    final documents = await Firestore.instance
        .collection('users')
        .document(userId)
        .collection('watchlist')
        .getDocuments();

    return documents.documents
        .map((json) => Movie().fromMap(json.data))
        .where((element) {
      return query?.isNotEmpty == true
          ? element.name.toLowerCase().contains(query.toLowerCase())
          : true;
    }).toList();
  }

  @override
  Future<MovieSearchResponse> findMovies(String query) async {
    return omdbClient.findMovies(query);
  }
}
